﻿CREATE TABLE [nilnul.acc_._rsced].[Rsc]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[uri] nvarchar(400)  --uri: urn, url; urc
	,
	_memo nvarchar(max)
	,
	_time datetime default getUtcDate()
)
