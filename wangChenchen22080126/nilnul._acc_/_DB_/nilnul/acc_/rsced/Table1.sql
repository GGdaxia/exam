﻿CREATE TABLE [nilnul.acc_].[Rsced]
/* resourced: an account with resource   */
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[rsc] bigint  references [nilnul.acc_._rsced].Rsc(id)
	,
	[name] nvarchar(400) 
	,
	[pass_tip] nvarchar(400)
	,_memo nvarchar(max)
	,_time datetime default getUtcDate()
)
