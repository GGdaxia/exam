﻿/*
ZhouRuiXue22080147 的部署脚本

此代码由工具生成。
如果重新生成此代码，则对此文件的更改可能导致
不正确的行为并将丢失。
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "ZhouRuiXue22080147"
:setvar DefaultFilePrefix "ZhouRuiXue22080147"
:setvar DefaultDataPath "C:\Users\L4M404N009\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"
:setvar DefaultLogPath "C:\Users\L4M404N009\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"

GO
:on error exit
GO
/*
请检测 SQLCMD 模式，如果不支持 SQLCMD 模式，请禁用脚本执行。
要在启用 SQLCMD 模式后重新启用脚本，请执行:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'要成功执行此脚本，必须启用 SQLCMD 模式。';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'正在创建 架构 [nilnul.acc_]...';


GO
CREATE SCHEMA [nilnul.acc_]
    AUTHORIZATION [dbo];


GO
PRINT N'正在创建 架构 [nilnul.acc_._rsced]...';


GO
CREATE SCHEMA [nilnul.acc_._rsced]
    AUTHORIZATION [dbo];


GO
PRINT N'正在创建 表 [nilnul.acc_].[Rsced]...';


GO
CREATE TABLE [nilnul.acc_].[Rsced] (
    [id]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [rsc]      BIGINT         NULL,
    [name]     NVARCHAR (400) NULL,
    [pass_tip] NVARCHAR (400) NULL,
    [_memo]    NVARCHAR (MAX) NULL,
    [_time]    DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
PRINT N'正在创建 表 [nilnul.acc_._rsced].[Rsc]...';


GO
CREATE TABLE [nilnul.acc_._rsced].[Rsc] (
    [id]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [uri]   NVARCHAR (400) NULL,
    [_memo] NVARCHAR (MAX) NULL,
    [_time] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
PRINT N'正在创建 默认约束 [nilnul.acc_].[Rsced] 上未命名的约束...';


GO
ALTER TABLE [nilnul.acc_].[Rsced]
    ADD DEFAULT getUtcDate() FOR [_time];


GO
PRINT N'正在创建 默认约束 [nilnul.acc_._rsced].[Rsc] 上未命名的约束...';


GO
ALTER TABLE [nilnul.acc_._rsced].[Rsc]
    ADD DEFAULT getUtcDate() FOR [_time];


GO
PRINT N'正在创建 外键 [nilnul.acc_].[Rsced] 上未命名的约束...';


GO
ALTER TABLE [nilnul.acc_].[Rsced] WITH NOCHECK
    ADD FOREIGN KEY ([rsc]) REFERENCES [nilnul.acc_._rsced].[Rsc] ([id]);


GO
PRINT N'根据新创建的约束检查现有的数据';


GO
USE [$(DatabaseName)];


GO
CREATE TABLE [#__checkStatus] (
    id           INT            IDENTITY (1, 1) PRIMARY KEY CLUSTERED,
    [Schema]     NVARCHAR (256),
    [Table]      NVARCHAR (256),
    [Constraint] NVARCHAR (256)
);

SET NOCOUNT ON;

DECLARE tableconstraintnames CURSOR LOCAL FORWARD_ONLY
    FOR SELECT SCHEMA_NAME([schema_id]),
               OBJECT_NAME([parent_object_id]),
               [name],
               0
        FROM   [sys].[objects]
        WHERE  [parent_object_id] IN (OBJECT_ID(N'nilnul.acc_.Rsced'))
               AND [type] IN (N'F', N'C')
                   AND [object_id] IN (SELECT [object_id]
                                       FROM   [sys].[check_constraints]
                                       WHERE  [is_not_trusted] <> 0
                                              AND [is_disabled] = 0
                                       UNION
                                       SELECT [object_id]
                                       FROM   [sys].[foreign_keys]
                                       WHERE  [is_not_trusted] <> 0
                                              AND [is_disabled] = 0);

DECLARE @schemaname AS NVARCHAR (256);

DECLARE @tablename AS NVARCHAR (256);

DECLARE @checkname AS NVARCHAR (256);

DECLARE @is_not_trusted AS INT;

DECLARE @statement AS NVARCHAR (1024);

BEGIN TRY
    OPEN tableconstraintnames;
    FETCH tableconstraintnames INTO @schemaname, @tablename, @checkname, @is_not_trusted;
    WHILE @@fetch_status = 0
        BEGIN
            PRINT N'检查约束:' + @checkname + N' [' + @schemaname + N'].[' + @tablename + N']';
            SET @statement = N'ALTER TABLE [' + @schemaname + N'].[' + @tablename + N'] WITH ' + CASE @is_not_trusted WHEN 0 THEN N'CHECK' ELSE N'NOCHECK' END + N' CHECK CONSTRAINT [' + @checkname + N']';
            BEGIN TRY
                EXECUTE [sp_executesql] @statement;
            END TRY
            BEGIN CATCH
                INSERT  [#__checkStatus] ([Schema], [Table], [Constraint])
                VALUES                  (@schemaname, @tablename, @checkname);
            END CATCH
            FETCH tableconstraintnames INTO @schemaname, @tablename, @checkname, @is_not_trusted;
        END
END TRY
BEGIN CATCH
    PRINT ERROR_MESSAGE();
END CATCH

IF CURSOR_STATUS(N'LOCAL', N'tableconstraintnames') >= 0
    CLOSE tableconstraintnames;

IF CURSOR_STATUS(N'LOCAL', N'tableconstraintnames') = -1
    DEALLOCATE tableconstraintnames;

SELECT N'约束验证失败:' + [Schema] + N'.' + [Table] + N',' + [Constraint]
FROM   [#__checkStatus];

IF @@ROWCOUNT > 0
    BEGIN
        DROP TABLE [#__checkStatus];
        RAISERROR (N'验证约束时出错', 16, 127);
    END

SET NOCOUNT OFF;

DROP TABLE [#__checkStatus];


GO
PRINT N'更新完成。';


GO
